﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {

        int bet = 0;
        Gracz graczJednorekiBandyta;
        public Form2(Gracz gracz)
        {
            InitializeComponent();
            graczJednorekiBandyta = gracz;
            UpdateMoneyText();
        }

        void UpdateMoneyText()
        {
            textBox4.Text = Singleton.Instance.ShowBankStatus().ToString();
            textBox5.Text = graczJednorekiBandyta.BetMoney.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bet = (int)numericUpDown1.Value;
            if (bet == 0)
            {
                MessageBox.Show("Nie możesz grać bez obstawienia zakładu");
                return;
            }
            playGame();
        }

        void ClearSlots()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        void playGame()
        {
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            

            Random rand = new Random();
            num1 = rand.Next(1, 4);
            textBox1.Text = num1.ToString();
            num2 = rand.Next(1, 4);
            textBox2.Text = num2.ToString();
            num3 = rand.Next(1, 4);
            textBox3.Text = num3.ToString();

            if(num1 == num2 && num2 == num3)
            {
                string message = "Wygrałeś!";
                string caption = "Wynik";
                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;

                result = MessageBox.Show(message, caption, buttonOK);
                graczJednorekiBandyta.BetMoney += bet;
                Singleton.Instance.RemoveFromBank(bet);
                UpdateMoneyText();
                ClearSlots();
            }
            else
            {
                string message = "Przegrałeś!";
                string caption = "Wynik";
                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;

                result = MessageBox.Show(message, caption, buttonOK);
                graczJednorekiBandyta.BetMoney -= bet;
                Singleton.Instance.AddToBank(bet);
                UpdateMoneyText();
                ClearSlots();
            }

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
