﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Gracz graczBlackjack = new Gracz() { BetMoney = 500 };
        Gracz graczJednorekiBandyta = new Gracz() { BetMoney = 500 };
        public Form1()
        {
            InitializeComponent();
            DrawIcon();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 jednoreki = new Form2(graczJednorekiBandyta);
            jednoreki.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 blackjack = new Form3(graczBlackjack);
            blackjack.Show();
        }

        void DrawIcon()
        {
            PictureBox picture = new PictureBox();
            picture.Name = "pictureBox";
            picture.Size = new Size(60, 60);
            picture.Location = new Point(145, 50);
            picture.Image = Image.FromFile("C:\\Users\\WysuW\\source\\repos\\WindowsFormsApp1\\WindowsFormsApp1\\Images\\logo.png");
            picture.Visible = true;
            Controls.Add(picture);
        }
    }
}
