﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        
        static Random rand = new Random();
        int index = 0;
        int currentCard = 1;
        int cardCPosX = 0;
        int cardPPosX = 0;
        int cardView = 1;
        int endPlayerSum = 0;
        int endCpuSum = 0;
        
        Card checkCurrent = new Card();
        Gracz graczBlackjack;
        String[] file = new string[52];
        Card[] deck = new Card[52];
        List<Card> playerDeck = new List<Card>();
        List<Card> cpuDeck = new List<Card>();
        List<PictureBox> cardViews = new List<PictureBox>();
        int bet = 0;

        public Form3(Gracz gracz)
        {
            InitializeComponent();
            graczBlackjack = gracz;
            UpdateMoneyText();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bet = (int)numericUpDown1.Value;
            if (bet == 0)
            {
                MessageBox.Show("Nie możesz grać bez obstawienia zakładu");
                return;
            }

            button1.Visible = true;
            button2.Visible = true;
            PopulateFileNameArray();
            GenerateDeck();
            giveCpuCards();
            givePlayerCard();
            givePlayerCard();
        }

        void PopulateFileNameArray()
        {
 
            int j = 1;
            for (int i =0; i < file.Length; i++)
            {
                string  path = Path.GetDirectoryName(Application.ExecutablePath);
                file[i] = path+"\\Images\\" + j + ".png";

                //  NIE RELATYWNE, POPRAWIONE POWYŻEJ  "C:\\Users\\WysuW\\source\\repos\\WindowsFormsApp1\\WindowsFormsApp1\\Images\\" +j +".png";
                j++;
            }
        }

        void GenerateDeck()
        {
            for(int i = 0; i < 4; i++)
            {
                for(int j = 1; j < 14; j++)
                {
                    
                    Card card = new Card();
                    if (j > 10)
                    {
                        card.value = 10;
                        card.cardImage = Image.FromFile(file[index]);
                    }
                    else
                    {
                        card.value = j;
                        card.cardImage = Image.FromFile(file[index]);
                    }
                    card.suit = i;


                    deck[index] = card;
                    index++;
                }
            }
            Shuffle(deck);
        }

        void giveCpuCards()
        {
            
            cpuDeck.Add(deck[currentCard]);
            AddCpuCardView(currentCard);
            currentCard++;
            checkCurrent = deck[currentCard];
            cpuDeck.Add(deck[currentCard]);
            currentCard++;
            checkCurrent = deck[currentCard];
        }

        void giveCpuCards1()
        {

            cpuDeck.Add(deck[currentCard]);
            AddCpuCardView(currentCard);
            currentCard++;
            CheckCpuSum();
        }

        void givePlayerCard()
        {
            
            playerDeck.Add(deck[currentCard]);
            AddPlayerCardView(currentCard);
            currentCard++;
            
            CheckPlayerSum();
        }

        void Shuffle(Card[] cardDeck)
        {
            for( int i = cardDeck.Length - 1; i > 0; i--)
            {
                int j = rand.Next(i + 1);
                Card temp = cardDeck[i];
                cardDeck[i] = cardDeck[j];
                cardDeck[j] = temp;
            }
        }

        void CheckCpuSum()
        {
            int cpuSum = 0;
            int aces = 0;
            foreach (Card card in cpuDeck)
            {
                if (card.value == 1)
                    aces++;
                cpuSum += card.value;
            }
            if (aces == 1 && cpuSum <= 11)
                cpuSum += 10;

            if (cpuSum <= endPlayerSum && cpuSum!=21)
                giveCpuCards1();
            if (cpuSum > endPlayerSum && cpuSum <= 21)
            {
                string message = "Przegrałeś "+bet;
                string caption = "Wynik";
                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;

                graczBlackjack.BetMoney -= bet;
                Singleton.Instance.AddToBank(bet);
                endCpuSum = cpuSum;
                UpdateMoneyText();
                label2.Text = endCpuSum.ToString();
                result = MessageBox.Show(message, caption, buttonOK);
                EndGame();
            }
            if (cpuSum > 21)
            {
                string message = "Wygrałeś "+bet;
                string caption = "Wynik";
                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;
                graczBlackjack.BetMoney += bet;
                Singleton.Instance.RemoveFromBank(bet);
                endCpuSum = cpuSum;
                UpdateMoneyText();
                label2.Text = endCpuSum.ToString();
                result = MessageBox.Show(message, caption, buttonOK);
                EndGame();
            }

            if(cpuSum == 21 && endPlayerSum == 21)
            {
                string message = "Remis ";
                string caption = "Wynik";
                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;
                endCpuSum = cpuSum;
                UpdateMoneyText();
                label2.Text = endCpuSum.ToString();
                result = MessageBox.Show(message, caption, buttonOK);
                EndGame();
            }

            

        }

        void UpdateMoneyText()
        {
            
            textBox2.Text = Singleton.Instance.ShowBankStatus().ToString();
            textBox1.Text = graczBlackjack.BetMoney.ToString();
        }

        void CheckPlayerSum()
        {
            int playerSum = 0;
            int aces = 0;
            foreach (Card card in playerDeck)
            {
                if (card.value == 1)
                    aces++;
                playerSum += card.value;
                
            }
            if (playerSum <= 11 && aces > 0)
                playerSum += 10;
            label1.Text = playerSum.ToString();

            if(playerSum >21)
            {
                string message = "Przegrałeś "+bet;
                string caption = "Wynik";

                DialogResult result;
                MessageBoxButtons buttonOK = MessageBoxButtons.OK;
                graczBlackjack.BetMoney -= bet;
                Singleton.Instance.AddToBank(bet);
                UpdateMoneyText();
                label2.Text = endCpuSum.ToString();
                result = MessageBox.Show(message, caption, buttonOK);
                EndGame();
            }

            endPlayerSum = playerSum;

        }

        void AddCpuCardView(int index)
        {


            PictureBox picture = new PictureBox();
            picture.Name = "pictureBox";
            picture.Size = new Size(50, 50);
            picture.Location = new Point(25+cardCPosX, 50);
            cardCPosX += 45;


            picture.Image = deck[index].cardImage;
            picture.Visible = true;
            cardViews.Add(picture);
            Controls.Add(picture);
        }

        void AddPlayerCardView(int index)
        {


            PictureBox picture = new PictureBox();
            picture.Name = "pictureBox"+cardView;
            picture.Size = new Size(50, 50);
            picture.Location = new Point(25+cardPPosX, 180);
            cardPPosX += 45;


            picture.Image = deck[index].cardImage;
            picture.Visible = true;
            cardViews.Add(picture);
            Controls.Add(picture);
        }


        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            givePlayerCard();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddCpuCardView(2);
            CheckCpuSum();
        }
        void DestroyCardViews()
        {
            foreach (PictureBox pic in cardViews)
            {
                pic.Dispose();
            }
        }

        void EndGame()
        {
             
            DestroyCardViews();
            this.Close();
        }

    }
}
