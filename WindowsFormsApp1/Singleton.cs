﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Singleton
    {
        private static Singleton instance;
        private int bankAccount = 0;

        private Singleton()
        {
            bankAccount = 1000;
        }

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }

        public void AddToBank(int money)
        {
            bankAccount += money;
        }

        public void RemoveFromBank(int money)
        {
            bankAccount -= money;
        }

        public int ShowBankStatus()
        {
            return bankAccount;
        }

    }

}
